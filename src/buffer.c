#include "buffer.h"
#include "types.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define IS_POWER_OF_TWO(x) ((x) > 1) && (((x) & ((x) - 1)) == 0)

void RingBufferInit(RingBuffer *rb, byte *data, int size)
{
    assert(IS_POWER_OF_TWO(size));
    RingBufferReset(rb);
    rb->size = size;
    rb->data = data;
}

void RingBufferReset(RingBuffer *rb)
{
    rb->start = 0;
    rb->end = 0;
}

bool RingBufferIsFull(RingBuffer *rb)
{
    return rb->end == (rb->start ^ rb->size);
}

bool RingBufferIsEmpty(RingBuffer *rb)
{
    return rb->end == rb->start;
}

int RingBufferIncrement(RingBuffer *rb, int p)
{
    return (p + 1) & (2 * rb->size - 1);
}

void RingBufferWrite(RingBuffer *rb, byte elem)
{
    rb->data[rb->end & (rb->size - 1)] = elem;
    
    if (RingBufferIsFull(rb)) {
        rb->start = RingBufferIncrement(rb, rb->start);
    }
    
    rb->end = RingBufferIncrement(rb, rb->end);
}

byte RingBufferRead(RingBuffer *rb)
{
    byte b = RingBufferPeek(rb);
    rb->start = RingBufferIncrement(rb, rb->start);
    return b;
}

byte RingBufferPeek(RingBuffer *rb)
{
    return rb->data[rb->start & (rb->size - 1)];
}
