#ifndef bot_parser_h
#define bot_parser_h

#include <stdbool.h>

typedef struct {
    char prefix[IRC_MSG_PREFIX_LEN_MAX + 1];
    char command[IRC_MSG_CMD_LEN_MAX + 1];
    char params[IRC_MSG_LEN_MAX - 6 + 1];
} IRCMessage;

int IRCMessageParse(RingBuffer *rb, IRCMessage *msg);

void IRCMessagePrint(IRCMessage *msg);

#endif
