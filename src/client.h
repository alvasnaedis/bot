#ifndef bot_client_h
#define bot_client_h

#include "buffer.h"

typedef struct {
    char nickname[IRC_MSG_NICK_LEN_MAX + 1];
    char realname[IRC_MSG_NAME_LEN_MAX + 1];
} IRCConfig;

typedef struct {
    int socket;
    RingBuffer in_buf;
    RingBuffer out_buf;
    byte in_data[IRC_MSG_LEN_MAX];
    byte out_data[IRC_MSG_LEN_MAX];
    IRCConfig config;
} IRC;

void IRCInit(IRC *irc, IRCConfig *config);

void IRCConnect(IRC *irc, const char *host, int port);

void IRCProcess(IRC *irc);

#endif
