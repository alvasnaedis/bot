#ifndef bot_circularbuffer_h
#define bot_circularbuffer_h

#include "types.h"

typedef struct {
    int size;
    int start;
    int end;
    byte *data;
} RingBuffer;

void RingBufferInit(RingBuffer *rb, byte *data, int size);

void RingBufferReset(RingBuffer *rb);

bool RingBufferIsFull(RingBuffer *rb);

bool RingBufferIsEmpty(RingBuffer *rb);

int RingBufferIncrement(RingBuffer *rb, int p);

void RingBufferWrite(RingBuffer *rb, byte elem);

byte RingBufferRead(RingBuffer *rb);

byte RingBufferPeek(RingBuffer *rb);

#endif
