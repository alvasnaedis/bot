#include "buffer.h"
#include "constants.h"
#include "parser.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

/*
 message    =  [ ":" prefix SPACE ] command [ params ] crlf
 prefix     =  servername / ( nickname [ [ "!" user ] "@" host ] )
 command    =  1*letter / 3digit
 params     =  *14( SPACE middle ) [ SPACE ":" trailing ]
 =/ 14( SPACE middle ) [ SPACE [ ":" ] trailing ]
 
 nospcrlfcl =  %x01-09 / %x0B-0C / %x0E-1F / %x21-39 / %x3B-FF
 ; any octet except NUL, CR, LF, " " and ":"
 middle     =  nospcrlfcl *( ":" / nospcrlfcl )
 trailing   =  *( ":" / " " / nospcrlfcl )
 
 SPACE      =  %x20        ; space character
 crlf       =  %x0D %x0A   ; "carriage return" "linefeed"
 */

int IRCMessageParseUntil(RingBuffer *rb, byte end, byte *dest, int len)
{
    for (int i = 0; i < len; ++i) {
        byte b = RingBufferRead(rb);

        if (b == end) {
            dest[i] = '\0';
            return i;
        }

        dest[i] = b;
    }

    return 0;
}

int IRCMessageParse(RingBuffer *rb, IRCMessage *msg)
{
    assert(!RingBufferIsEmpty(rb));
    byte b = RingBufferPeek(rb);
    int parsed = 0;

    if (b == IRC_MSG_PREFIX) {
        parsed += IRCMessageParseUntil(rb, IRC_MSG_SEP, msg->prefix, sizeof(msg->prefix));
    }

    parsed += IRCMessageParseUntil(rb, IRC_MSG_SEP, msg->command, sizeof(msg->command));
    parsed += IRCMessageParseUntil(rb, IRC_MSG_RET, msg->params, sizeof(msg->params));
    b = RingBufferRead(rb); // Chomp off \n
    assert(b == IRC_MSG_END);

    return ++parsed;
}

void IRCMessagePrint(IRCMessage *msg)
{
    printf("prefix=%s command=%s params=%s\n", msg->prefix, msg->command, msg->params);
}
