#include "client.h"
#include "parser.h"

#include <arpa/inet.h>
#include <assert.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#define IRC_TIMEOUT 1000

#define IS_CMD(a, b) (strcmp((a), (b)) == 0)
//bool is_cmd(const char *a, const char *b) { return strcmp(a, b) == 0; }

void IRCWrite(IRC *irc, const byte *data, int len)
{
    for (int i = 0; i < len; ++i) {
        RingBufferWrite(&irc->out_buf, data[i]);
    }
}

void IRCWriteString(IRC *irc, const byte *str)
{
    IRCWrite(irc, str, (int)strlen(str));
}

void IRCWriteEnd(IRC *irc)
{
    IRCWriteString(irc, "\r\n");

    printf("Wrote: ");

    while (!RingBufferIsEmpty(&irc->out_buf)) {
        byte b = RingBufferRead(&irc->out_buf);
        write(irc->socket, &b, 1);
        printf("%c", b);
    }

    printf("\n");
}

void IRCInit(IRC *irc, IRCConfig *config)
{
    irc->config = *config;
    irc->socket = socket(AF_INET, SOCK_STREAM, 0);
    assert(irc->socket >= 0);
    RingBufferInit(&irc->in_buf, (byte *)&irc->in_data, IRC_MSG_LEN_MAX);
    RingBufferInit(&irc->out_buf, (byte *)&irc->out_data, IRC_MSG_LEN_MAX);
}

void IRCConnect(IRC *irc, const char *host, int port)
{
    struct sockaddr_in serv_addr;
    int res = 0;
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    res = inet_pton(AF_INET, host, &serv_addr.sin_addr);
    assert(res > 0);
    res = connect(irc->socket, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
    assert(res >= 0);
    IRCWriteString(irc, CMD_NICK);
    IRCWriteString(irc, " ");
    IRCWriteString(irc, irc->config.nickname);
    IRCWriteEnd(irc);
    IRCWriteString(irc, CMD_USER);
    IRCWriteString(irc, " ");
    IRCWriteString(irc, irc->config.nickname);
    IRCWriteString(irc, " 0 * :");
    IRCWriteString(irc, irc->config.realname);
    IRCWriteEnd(irc);
    IRCWriteString(irc, "PRIVMSG jona :Hej heheheh ja e C    robot");
    IRCWriteEnd(irc);
}

void IRCMessageProcess(IRC *irc, IRCMessage *msg)
{
    if (IS_CMD(msg->command, CMD_PING)) {
        IRCWrite(irc, CMD_PONG, strlen(CMD_PONG));
        IRCWrite(irc, " ", 1);
        IRCWriteString(irc, msg->params);
        IRCWriteEnd(irc);
    } else {
        printf("Unhandled message: %s\n", msg->command);
    }
}

void IRCProcess(IRC *irc)
{
    struct timeval tv;
    tv.tv_sec = IRC_TIMEOUT;
    tv.tv_usec = 0;
    fd_set read_fds;
    FD_ZERO(&read_fds);
    FD_SET(irc->socket, &read_fds);

    for (;;) {
        IRCMessage msg = {0};
        int av = 0, r = 0;
        int nr = select(irc->socket + 1, &read_fds, NULL, NULL, &tv);

        if (nr <= 0 || !FD_ISSET(irc->socket, &read_fds)) {
            if (nr == -1) { perror("select"); }
            continue;
        }

        ioctl(irc->socket, FIONREAD, &av);

        if (av == 0) {
            puts("Connection closed.");
            return;
        }

        for (int i = 0; i < av; ++i) {
            if (!RingBufferIsFull(&irc->in_buf)) {
                byte b;
                int n = (int)read(irc->socket, &b, 1);
                assert(n == 1);
                RingBufferWrite(&irc->in_buf, b);

                if (b == IRC_MSG_END) {
                    break;
                }
            } else {
                break;
            }
        }

        if ((r = IRCMessageParse(&irc->in_buf, &msg)) > 0) {
            IRCMessagePrint(&msg);
            IRCMessageProcess(irc, &msg);
        } else {
            RingBufferReset(&irc->in_buf);
            puts("IRCMessageParse failed.");
        }
    }
}
