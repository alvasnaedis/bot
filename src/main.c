#include "client.h"

#include <stdio.h>
#include <stdlib.h>

#define REQUIRED_ARGS 3

void usage()
{
    puts("Please provide a hostname and port");
}

int main(int argc, const char *argv[])
{
    IRC irc;
    const char *ip;
    int port;

    if (argc < REQUIRED_ARGS) {
        usage();
        return EXIT_FAILURE;
    }

    ip = argv[1];
    port = atoi(argv[2]);

    IRCConfig config = {
        .nickname = "c-ot",
        .realname = "C Bot"
    };

    IRCInit(&irc, &config);
    IRCConnect(&irc, ip, port);
    IRCProcess(&irc);

    return EXIT_SUCCESS;
}
