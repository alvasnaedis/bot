#ifndef bot_constants_h
#define bot_constants_h

// Max sizes.
#define IRC_MSG_LEN_MAX 0x200
#define IRC_MSG_PREFIX_LEN_MAX 0x80
#define IRC_MSG_NAME_LEN_MAX 0x40
#define IRC_MSG_NICK_LEN_MAX 0x10
#define IRC_MSG_CMD_LEN_MAX 0x8

// Message stuff.
#define IRC_MSG_PREFIX ':'
#define IRC_MSG_SEP ' '
#define IRC_MSG_RET '\r'
#define IRC_MSG_END '\n'

// Commands.
#define CMD_NICK "NICK"
#define CMD_PING "PING"
#define CMD_PONG "PONG"
#define CMD_USER "USER"

#endif
